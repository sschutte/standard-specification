[[anchor-1]]
== Introduction

This document describes a standard for exchanging data in the tourism domain, called *{AlpineBitsR}*, which is built upon:

* an link:++https://en.wikipedia.org/wiki/OntoUML++[OntoUML] ontology that describes the conceptualization and scope of the standard;

* the link:++https://en.wikipedia.org/wiki/Representational_state_transfer++[REST architectural style];

* the link:++https://jsonapi.org/format/1.0/++[{JSONAPI}] specification for REST APIs that exchange JSON data through HTTP messages;

* link:++https://en.wikipedia.org/wiki/HTTPS++[HTTPS] and link:++https://en.wikipedia.org/wiki/Basic_access_authentication++[basic authentication] protocols for secure communication;

* the link:++https://json-schema.org/++[JSON Schema] standard, link:++https://tools.ietf.org/html/draft-handrews-json-schema-01++[draft 7] for message validation;

* the link:++https://tools.ietf.org/html/rfc7946++[GeoJSON] standard for JSON geospatial modelling; and

* link:++https://en.wikipedia.org/wiki/Schema.org++[Schema.org], used as an inspiration for designing resource types.

* the link:https://www.openapis.org/[OpenAPI] specification, recommended as an additional standard for documentation of server implementations.

The *{AlpineBitsR}* standard was designed to support data exchange between systems acting as CLIENTS and SERVER, where CLIENTS consume the data provided by SERVERS.

The *{AlpineBitsR}* standard defines:

* resource types

* server endpoints

* support to GET requests

* request and response headers and parameters

* additional request features (e.g., pagination and hypermedia controls)

The current version of the standard supports exchanging data about events, event series, event venues, mountain areas, lifts, ski slopes, snowparks, agents, and media objects.


[[anchor-1.1]]
=== Conventions and Definitions

The keywords “MUST”, “MUST NOT”, “REQUIRED”, “SHALL", “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in https://tools.ietf.org/html/rfc2119[RFC 2119].

Additionally, the following terminology is consistently used throughout this document:

* `CLIENT`: a system that consumes data provided by a SERVER via HTTP requests

* `SERVER`: a system that stores and exposes data to CLIENTS in conformance with the *{AlpineBitsR}* API.

* `Resource Type`: a set of attributes and relationships used to characterize resources

* `Resource`: an object with a persistent identifier that conforms to a resource type

* `Endpoint`: the trailing part of a URL that provides access to HTTP requests on an API
